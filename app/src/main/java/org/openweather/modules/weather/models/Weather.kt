package org.openweather.modules.weather.models

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
import org.openweather.modules.current.models.Current
import org.openweather.modules.daily.models.Daily
import org.openweather.modules.hourly.models.Hourly

@Parcelize
@JsonClass(generateAdapter = true)
data class Weather(

    @Json(name = "lat")
    val lat: Double? = null,

    @Json(name = "lon")
    val lon: Double? = null,

    @Json(name = "timezone")
    val timezone: String? = null,

    @Json(name = "timezone_offset")
    val timezone_offset: Int? = null,

    @Json(name = "current")
    val current: Current? = null,

    @Json(name = "hourly")
    val hourly: List<Hourly>? = null,

    @Json(name = "daily")
    val daily: List<Daily>? = null

) : Parcelable
