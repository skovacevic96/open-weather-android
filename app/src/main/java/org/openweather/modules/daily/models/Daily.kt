package org.openweather.modules.daily.models

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
import org.openweather.modules.feelslike.models.FeelsLike
import org.openweather.modules.temp.models.Temp
import org.openweather.modules.weather.models.Weather

@Parcelize
@JsonClass(generateAdapter = true)
data class Daily (

    @Json(name = "dt")
    val dt : Int,

    @Json(name = "sunrise")
    val sunrise : Int,

    @Json(name = "sunset")
    val sunset : Int,

    @Json(name = "temp")
    val temp : Temp,

    @Json(name = "feels_like")
    val feels_like : FeelsLike,

    @Json(name = "pressure")
    val pressure : Int,

    @Json(name = "humidity")
    val humidity : Int,

    @Json(name = "dew_point")
    val dew_point : Double,

    @Json(name = "wind_speed")
    val wind_speed : Double,

    @Json(name = "wind_deg")
    val wind_deg : Int,

    @Json(name = "weather")
    val weather : List<Weather>,

    @Json(name = "clouds")
    val clouds : Int,

    @Json(name = "uvi")
    val uvi : Double
) : Parcelable