package org.openweather.modules.current.models

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
import org.openweather.modules.weather.models.Weather

@Parcelize
@JsonClass(generateAdapter = true)
data class Current(

    @Json(name = "dt")
    val dt: Int? = null,

    @Json(name = "sunrise")
    val sunrise: Int? = null,

    @Json(name = "sunset")
    val sunset: Int? = null,

    @Json(name = "temp")
    val temp: Double? = null,

    @Json(name = "feels_like")
    val feels_like: Double? = null,

    @Json(name = "pressure")
    val pressure: Int? = null,

    @Json(name = "humidity")
    val humidity: Int? = null,

    @Json(name = "dew_point")
    val dew_point: Double? = null,

    @Json(name = "uvi")
    val uvi: Double? = null,

    @Json(name = "clouds")
    val clouds: Int? = null,

    @Json(name = "visibility")
    val visibility: Int? = null,

    @Json(name = "wind_speed")
    val wind_speed: Double? = null,

    @Json(name = "wind_deg")
    val wind_deg: Int? = null,

    @Json(name = "wind_gust")
    val wind_gust: Double? = null,

    @Json(name = "weather")
    val weather: List<Weather>? = null
) : Parcelable
