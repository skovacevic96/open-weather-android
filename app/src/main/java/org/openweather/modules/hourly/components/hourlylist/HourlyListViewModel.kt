package org.openweather.modules.hourly.components.hourlylist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.openweather.modules.hourly.models.Hourly
import org.openweather.modules.openweather.servicelayer.OpenWeatherRepository
import org.openweather.services.error.AuthenticationException
import org.openweather.services.error.NoInternetException
import org.openweather.services.persistence.SharedPreferences
import org.openweather.utils.enums.ErrorStates
import org.openweather.utils.enums.ErrorStates.*
import javax.inject.Inject

@HiltViewModel
class HourlyListViewModel @Inject constructor(
    private val repository: OpenWeatherRepository,
    private val prefs: SharedPreferences
) : ViewModel() {

    private val _hourlyListFlow = MutableSharedFlow<List<Hourly>?>()
    val hourlyListFlow = _hourlyListFlow.asSharedFlow()

    private val _errorFlow = MutableSharedFlow<ErrorStates>()
    val errorFlow = _errorFlow.asSharedFlow()

    private val handler = CoroutineExceptionHandler { _, exception ->
        viewModelScope.launch {
            _errorFlow.emit(
                when (exception) {
                    is AuthenticationException -> AUTHENTICATION_ERROR
                    is NoInternetException -> NO_INTERNET_CONNECTION
                    else -> UNKNOWN_ERROR
                }
            )
        }

    }

    init {
        fetchData()
    }

    private fun getList(lat: Double, lon: Double, appId: String) {
        viewModelScope.launch(handler) {
            repository.getOpenWeather(lat, lon, appId).collect {
                _hourlyListFlow.emit(it?.hourly)
            }
        }
    }

    fun fetchData() {
        prefs.accessToken?.let {
            getList(42.4304, 19.2594, it)
        } ?: throw IllegalStateException("appId can't be null!")
    }


}