package org.openweather.modules.hourly.components.hourlylist

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import org.openweather.databinding.HourlyListFragmentBinding
import org.openweather.modules.hourly.adapters.HourlyListRecyclerViewAdapter
import org.openweather.modules.hourly.models.Hourly
import org.openweather.utils.enums.AppAction
import org.openweather.utils.enums.ErrorStates.AUTHENTICATION_ERROR
import org.openweather.utils.enums.ErrorStates.NO_INTERNET_CONNECTION
import org.openweather.utils.listeners.OnListItemInteractionListener

@AndroidEntryPoint
class HourlyListFragment : Fragment(), OnListItemInteractionListener<Hourly> {

    private var binding: HourlyListFragmentBinding? = null
    private val hourlyListRecyclerViewAdapter = HourlyListRecyclerViewAdapter(this)
    private val viewModel by viewModels<HourlyListViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = HourlyListFragmentBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        instantiateUi()
        observeViewModel()
    }

    private fun instantiateUi() {
        binding?.apply {
            hourlyListRv.adapter = hourlyListRecyclerViewAdapter
            swipeRefreshLayout.setOnRefreshListener {
                viewModel.fetchData()
                swipeRefreshLayout.isRefreshing = false
            }
        }
    }

    private fun observeViewModel() {
        lifecycleScope.launchWhenCreated {
            viewModel.errorFlow.collectLatest {
                showDialog(
                    when (it) {
                        NO_INTERNET_CONNECTION -> "No Internet connection."
                        AUTHENTICATION_ERROR -> "Invalid API key."
                        else -> "Unknown error occurred"
                    }
                )
            }
        }
        lifecycleScope.launchWhenCreated {
            viewModel.hourlyListFlow.collectLatest {
                it?.let {
                    hourlyListRecyclerViewAdapter.update(it)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    override fun onListItemInteraction(item: Hourly, appAction: AppAction) {
        findNavController().navigate(
            HourlyListFragmentDirections.actionHourlyListFragmentToHourlyFragment(
                hourly = item
            )
        )
    }

    private fun showDialog(message: String) {
        AlertDialog.Builder(requireContext()).setTitle(null)
            .setMessage(message)
            .setPositiveButton(
                "OK"
            ) { dialog, _ ->
                dialog.dismiss()
            }.show()
    }


}