package org.openweather.modules.hourly.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.openweather.databinding.HourlyItemBinding
import org.openweather.modules.hourly.models.Hourly
import org.openweather.utils.enums.AppAction
import org.openweather.utils.listeners.OnListItemInteractionListener

class HourlyListRecyclerViewAdapter(private val onListItemInteractionListener: OnListItemInteractionListener<Hourly>) :
    RecyclerView.Adapter<HourlyListRecyclerViewAdapter.HourlyItemViewHolder>() {

    private val list = ArrayList<Hourly>()

    fun update(newList: List<Hourly>) {
        list.clear()
        list.addAll(newList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HourlyItemViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        val binding = HourlyItemBinding
            .inflate(inflater, parent, false)
        return HourlyItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: HourlyItemViewHolder, position: Int) {
        holder.bind(list[position], onListItemInteractionListener)
    }

    override fun getItemCount() = list.size

    inner class HourlyItemViewHolder(private val binding: HourlyItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Hourly, listener: OnListItemInteractionListener<Hourly>) {
            binding.dt.text = item.dt.toString()
            binding.temp.text = item.temp.toString()
            binding.hourlyItem.setOnClickListener {
                listener.onListItemInteraction(item, AppAction.CLICK)
            }
        }
    }

}