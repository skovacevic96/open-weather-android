package org.openweather.modules.hourly.models

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
import org.openweather.modules.weather.models.Weather

@Parcelize
@JsonClass(generateAdapter = true)
data class Hourly (

    @Json(name = "dt")
    var dt : Int,

    @Json(name = "temp")
    val temp : Double,

    @Json(name = "feels_like")
    val feels_like : Double,

    @Json(name = "pressure")
    val pressure : Int,

    @Json(name = "humidity")
    val humidity : Int,

    @Json(name = "dew_point")
    val dew_point : Double,

    @Json(name = "clouds")
    val clouds : Int,

    @Json(name = "wind_speed")
    val wind_speed : Double,

    @Json(name = "wind_deg")
    val wind_deg : Int,

    @Json(name = "weather")
    val weather : List<Weather>
) : Parcelable

