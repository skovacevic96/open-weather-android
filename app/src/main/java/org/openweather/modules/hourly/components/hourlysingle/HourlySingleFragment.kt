package org.openweather.modules.hourly.components.hourlysingle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import org.openweather.databinding.HourlySingleFragmentBinding
import org.openweather.modules.hourly.models.Hourly

class HourlySingleFragment : Fragment() {


    private var binding: HourlySingleFragmentBinding? = null
    private var hourly: Hourly? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = HourlySingleFragmentBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hourly = arguments?.let {
            HourlySingleFragmentArgs.fromBundle(it).hourly
        }
        instantiateUi()
    }

    private fun instantiateUi() {
        hourly?.let {
            binding?.hourlySingleFragmentDt?.text = it.dt.toString()
            binding?.hourlySingleFragmentTemp?.text = it.temp.toString()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

}