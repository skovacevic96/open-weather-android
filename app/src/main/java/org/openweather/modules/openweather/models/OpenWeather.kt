package org.openweather.modules.openweather.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import org.openweather.modules.current.models.Current
import org.openweather.modules.daily.models.Daily
import org.openweather.modules.hourly.models.Hourly

@JsonClass(generateAdapter = true)
data class OpenWeather(
    @Json(name = "lat")
    val lat: Double,

    @Json(name = "lon")
    val lon: Double,

    @Json(name = "timezone")
    val timezone : String,

    @Json(name = "timezone_offset")
    val timezone_offset : Int,

    @Json(name = "current")
    val current : Current,

    @Json(name = "hourly")
    val hourly : List<Hourly>,

    @Json(name = "daily")
    val daily : List<Daily>
)
