package org.openweather.modules.openweather.servicelayer

import org.openweather.services.error.ErrorManager
import org.openweather.services.network.api.ApiService
import org.openweather.services.network.api.BaseRemote
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OpenWeatherRemote @Inject constructor(
    private val apiService: ApiService,
    errorManager: ErrorManager
) : BaseRemote(errorManager) {

    suspend fun getOpenWeather(
        lat: Double,
        lon: Double,
        appId: String
    ) = parseResult { apiService.get(lat, lon, appId) }

}