package org.openweather.modules.openweather.servicelayer

import org.openweather.services.error.NoInternetException
import org.openweather.services.network.NetManager
import org.openweather.services.network.api.BaseRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OpenWeatherRepository @Inject constructor(
    private val remote: OpenWeatherRemote,
    private val netManager: NetManager
) : BaseRepository() {

    fun getOpenWeather(
        lat: Double,
        lon: Double,
        appId: String
    ) = retrieveResourceAsFlow {
        if (netManager.isConnectedToInternet()) {
            remote.getOpenWeather(lat, lon, appId)
        } else {
            throw NoInternetException()
        }
    }

}