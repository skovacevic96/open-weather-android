package org.openweather.utils.enums

enum class ErrorStates {
    AUTHENTICATION_ERROR,
    NO_INTERNET_CONNECTION,
    UNKNOWN_ERROR
}