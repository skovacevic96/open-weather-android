package org.openweather.utils

object Constants {

    const val AUTHENTICATION_MESSAGE = "Invalid API key. Please see http://openweathermap.org/faq#error401 for more info."

}