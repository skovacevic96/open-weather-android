package org.openweather.utils.listeners

import org.openweather.utils.enums.AppAction

interface OnListItemInteractionListener<T> {
    fun onListItemInteraction(item: T, appAction: AppAction)
}