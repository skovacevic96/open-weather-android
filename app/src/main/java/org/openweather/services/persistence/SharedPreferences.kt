package org.openweather.services.persistence

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPreferences @Inject constructor(@ApplicationContext private val context: Context) {

    companion object {
        private const val PREF_FILENAME = "org.openweather"
        private const val ACCESS_TOKEN = "ACCESS_TOKEN"
    }

    private val prefs = context.getSharedPreferences(PREF_FILENAME, Context.MODE_PRIVATE)


    val accessToken: String?
        get() = prefs.getString(ACCESS_TOKEN, "83829897fc2dcde9fd8d3ae957718ac3")
}