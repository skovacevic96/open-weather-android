package org.openweather.services

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import org.openweather.BuildConfig
import timber.log.Timber

@HiltAndroidApp
class OpenWeatherApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

}