package org.openweather.services.error

class NoInternetException: Exception()

class UnknownErrorException: Exception()

class AuthenticationException: Exception()