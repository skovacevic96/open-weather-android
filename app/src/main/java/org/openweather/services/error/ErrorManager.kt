package org.openweather.services.error

import com.squareup.moshi.Moshi
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ErrorManager @Inject constructor(private val moshi: Moshi) {

    companion object {
        private const val AUTHENTICATION_MESSAGE =
            "Invalid API key. Please see http://openweathermap.org/faq#error401 for more info."
    }

    fun getAppError(errorJson: String?) {
        val adapter = moshi.adapter(ErrorResponse::class.java)
        if (errorJson == null) throw UnknownErrorException()
        val errorResponse = adapter.fromJson(errorJson)
        errorResponse?.let {
            throw when (it.cod) {
                401 -> {
                    throw when (it.message) {
                        AUTHENTICATION_MESSAGE -> AuthenticationException()
                        else -> UnknownErrorException()
                    }
                }
                else -> UnknownErrorException()
            }
        }
    }
}