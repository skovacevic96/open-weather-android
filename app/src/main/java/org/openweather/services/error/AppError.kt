package org.openweather.services.error

enum class AppError {
    GENERAL,
    UNKNOWN_ERROR,
    INTERNET,
    AUTHENTICATION
}