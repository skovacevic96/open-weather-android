package org.openweather.services.network.api

import org.openweather.services.OpenWeatherApplication
import org.openweather.services.error.ErrorManager
import retrofit2.Response

open class BaseRemote(private val errorManager : ErrorManager) {


    @Suppress("BlockingMethodInNonBlockingContext")
    protected suspend fun <T> parseResult(networkCall: suspend () -> Response<T>): T? {
        val response = networkCall.invoke()
        if (!response.isSuccessful) {
            val errorBody = response.errorBody()?.string()
            errorManager.getAppError(errorBody)
        }
        return response.body()
    }


}